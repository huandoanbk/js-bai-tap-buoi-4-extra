/**
 * Bài 1
 */
function isNamnhuan(nam) {
  if (
    (nam % 4 === 0 && nam % 100 !== 0 && nam % 400 !== 0) ||
    (nam % 100 === 0 && nam % 400 === 0)
  )
    return true;
  else return false;
}
function isValid(ngay, thang, nam) {
  if (nam < 1920) {
    document.getElementById("showNgay").innerText = "";
    alert("Năm cần lớn hơn 1920");
    return false;
  } else {
    switch (thang) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        if (ngay < 1 || ngay > 31 || ngay == "") {
          alert("Dữ liệu không hợp lệ");
          return false;
        } else return true;
        break;
      case 2:
        if (isNamnhuan(nam)) {
          if (ngay < 1 || ngay > 29 || ngay == "") {
            alert("Dữ liệu không hợp lệ");
            return false;
          } else return true;
        }
        if (ngay < 1 || ngay > 28 || ngay == "") {
          alert("Dữ liệu không hợp lệ");
          return false;
        } else return true;
      case 4:
      case 6:
      case 9:
      case 11:
        if (ngay < 1 || ngay > 30 || ngay == "") {
          alert("Dữ liệu không hợp lệ");
          return false;
        } else return true;
        break;
      default:
        alert("Dữ liệu không hợp lệ");
        return false;
    }
  }
}
function ngayMai() {
  var homNay = document.getElementById("homNay").value;
  var toDay = new Date(homNay);
  var ngay = toDay.getDate();
  var thang = toDay.getMonth() + 1;
  var nam = toDay.getFullYear();
  if (isValid(ngay, thang, nam)) {
    var ngayMai = new Date(homNay);
    ngayMai.setDate(toDay.getDate() + 1);
    document.getElementById(
      "showNgay"
    ).innerText = `Ngày mai: ${ngayMai.getDate()}/${
      ngayMai.getMonth() + 1
    }/${ngayMai.getFullYear()}`;
  }
}
function homQua() {
  var homNay = document.getElementById("homNay").value;
  var toDay = new Date(homNay);
  var ngay = toDay.getDate();
  var thang = toDay.getMonth() + 1;
  var nam = toDay.getFullYear();
  if (isValid(ngay, thang, nam)) {
    var homQua = new Date(homNay);
    homQua.setDate(toDay.getDate() - 1);
    document.getElementById(
      "showNgay"
    ).innerText = `Hôm qua: ${homQua.getDate()}/${
      homQua.getMonth() + 1
    }/${homQua.getFullYear()}`;
  }
}

/**
 * Bài 2
 * Tái sử dụng hàm kiểm tra năm nhuận của Bài 1
 */

function tinhSongay() {
  var thangB2 = document.getElementById("thangB2").value * 1;
  var namB2 = document.getElementById("namB2").value * 1;
  if (namB2 < 1920) {
    document.getElementById("soNgay").innerText = "";
    alert("Năm cần lớn hơn 1920");
  } else {
    switch (thangB2) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        document.getElementById(
          "soNgay"
        ).innerText = `Tháng ${thangB2} Năm ${namB2} có 31 ngày`;
        break;
      case 2:
        if (isNamnhuan(namB2)) {
          document.getElementById(
            "soNgay"
          ).innerText = `Tháng ${thangB2} Năm ${namB2} có 29 ngày`;
        } else
          document.getElementById(
            "soNgay"
          ).innerText = `Tháng ${thangB2} Năm ${namB2} có 28 ngày`;
        break;
      case 4:
      case 6:
      case 9:
      case 11:
        document.getElementById(
          "soNgay"
        ).innerText = `Tháng ${thangB2} Năm ${namB2} có 30 ngày`;
        break;
      default:
        document.getElementById("soNgay").innerText = "";
        alert("Dữ liệu không hợp lệ");
        return false;
    }
  }
}
/**
 * Bài 3
 */

function docSo() {
  var soDoc = document.getElementById("soDoc").value * 1;
  var hangTram, hangChuc, hangDonvi;
  var soChuc = soDoc % 100;
  hangDonvi = soChuc % 10;
  hangChuc = (soChuc - hangDonvi) / 10;
  hangTram = (soDoc - soChuc) / 100;
  var t = "";
  var c = "";
  var dv = "";
  if (soDoc > 99 && soDoc < 1000 && Number.isInteger(soDoc)) {
    switch (hangTram) {
      case 1:
        t = "Một trăm";
        break;
      case 2:
        t = "Hai trăm";
        break;
      case 3:
        t = "Ba trăm";
        break;
      case 4:
        t = "Bốn trăm";
        break;
      case 5:
        t = "Năm trăm";
        break;
      case 6:
        t = "Sáu trăm";
        break;
      case 7:
        t = "Bảy trăm";
        break;
      case 8:
        t = "Tám trăm";
        break;
      case 9:
        t = "Chín trăm";
        break;
      default:
        alert("Dữ liệu không hợp lệ");
    }
    switch (hangChuc) {
      case 0:
        if (hangDonvi == 0) {
          c = "";
        } else c = "lẻ";
        break;
      case 1:
        c = "mười";
        break;
      case 2:
        c = "hai mươi";
        break;
      case 3:
        c = "ba mươi";
        break;
      case 4:
        c = "bốn mươi";
        break;
      case 5:
        c = "năm mươi";
        break;
      case 6:
        c = "sáu mươi";
        break;
      case 7:
        c = "bảy mươi";
        break;
      case 8:
        c = "tám mươi";
        break;
      case 9:
        c = "chín mươi";
        break;
      default:
        alert("Dữ liệu không hợp lệ");
    }
    switch (hangDonvi) {
      case 0:
        dv = "";
        break;
      case 1:
        if (hangChuc == 0 || hangChuc == 1) {
          dv = "một";
        } else dv = "mốt";
        break;
      case 2:
        dv = "hai";
        break;
      case 3:
        dv = "ba";
        break;
      case 4:
        dv = "bốn";
        break;
      case 5:
        if (hangChuc == 0) {
          dv = "năm";
        } else dv = "lăm";
        break;
      case 6:
        dv = "sáu";
        break;
      case 7:
        dv = "bảy";
        break;
      case 8:
        dv = "tám";
        break;
      case 9:
        dv = "chín";
        break;
      default:
        alert("Dữ liệu không hợp lệ");
    }
  } else alert("Vui lòng nhập số nguyên dương có 3 chữ số");
  document.getElementById("docSo").innerText = `${t} ${c} ${dv}`;
}

/**
 * Bài 4
 */

function timNgannhat() {
  var tenSV1 = document.getElementById("tenSV1").value;
  var xSV1 = document.getElementById("xSV1").value;
  var ySV1 = document.getElementById("ySV1").value;

  var tenSV2 = document.getElementById("tenSV2").value;
  var xSV2 = document.getElementById("xSV2").value;
  var ySV2 = document.getElementById("ySV2").value;

  var tenSV3 = document.getElementById("tenSV3").value;
  var xSV3 = document.getElementById("xSV3").value;
  var ySV3 = document.getElementById("ySV3").value;

  var xTruong = document.getElementById("xTruong").value;
  var yTruong = document.getElementById("yTruong").value;
  if (
    tenSV1 == "" ||
    xSV1 == "" ||
    ySV1 == "" ||
    tenSV2 == "" ||
    xSV2 == "" ||
    ySV2 == "" ||
    tenSV3 == "" ||
    xSV3 == "" ||
    ySV3 == "" ||
    xTruong == "" ||
    yTruong == ""
  ) {
    alert("Vui lòng nhập đầy đủ dữ liệu");
    document.getElementById("sinhVien").innerText = "";
  } else {
    xSV1 = xSV1 * 1;
    ySV1 = ySV1 * 1;
    xSV2 = xSV2 * 1;
    ySV2 = ySV2 * 1;
    xSV3 = xSV3 * 1;
    ySV3 = ySV3 * 1;
    xTruong = xTruong * 1;
    yTruong = yTruong * 1;
    var d1 = khoangCach(xTruong, yTruong, xSV1, ySV1);
    var d2 = khoangCach(xTruong, yTruong, xSV2, ySV2);
    var d3 = khoangCach(xTruong, yTruong, xSV3, ySV3);

    if (d1 > d2) {
      if (d1 > d3)
        document.getElementById(
          "sinhVien"
        ).innerText = `Sinh viên xa nhất trường: ${tenSV1}`;
      else
        document.getElementById(
          "sinhVien"
        ).innerText = `Sinh viên xa nhất trường: ${tenSV3}`;
    } else if (d2 > d3)
      document.getElementById(
        "sinhVien"
      ).innerText = `Sinh viên xa nhất trường: ${tenSV2}`;
    else
      document.getElementById(
        "sinhVien"
      ).innerText = `Sinh viên xa nhất trường: ${tenSV3}`;
  }
}
function khoangCach(x1, y1, x2, y2) {
  return (d = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
}
